import { ApolloClient } from "apollo-client";
import { ApolloLink } from "apollo-link";
import { createHttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { setContext } from "apollo-link-context";
import { onError } from "apollo-link-error";
import { auth } from "@/firebase/firebase";
import Vue from "vue";

import VueApollo from "vue-apollo";

let token: string;
// HTTP connection to the API
const crmPoint = createHttpLink({ uri: process.env.VUE_APP_CRM_BACKEND_ENDPOINT });
const portalPoint = createHttpLink({
	uri: process.env.VUE_APP_PORTAL_BACKEND_ENDPOINT
});
const endpointLink = ApolloLink.split(
	operation => operation.getContext().clientName === "portal",
	portalPoint,
	crmPoint
);

const withToken = setContext(async () => {
	// if you have a cached value, return it immediately
	return {
		headers: {
			authorization: await auth.currentUser?.getIdToken()
		}
	};
});

const resetToken = onError(({ networkError }) => {
	if (networkError && networkError.name === "ServerError") {
		// remove cached token on 401 from the server
		token = "";
	}
});

const authFlowLink = withToken.concat(resetToken);

// Cache implementation
const cache = new InMemoryCache();

// Create the apollo client
const apolloClient = new ApolloClient({
	link: authFlowLink.concat(endpointLink),
	cache
});

export default new VueApollo({
	defaultClient: apolloClient
});

Vue.use(VueApollo);
