import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./assets/css/index.css";
import "@zudoinnovations/edvoy-ui/dist/edvoy-ui.min.css";
import { auth } from "./firebase/firebase";
import apolloProvider from "@/apollo/index";
Vue.config.productionTip = false;

import Vuelidate from "vuelidate";
Vue.use(Vuelidate);

import "vue-class-component/hooks";

let app: string | any;
auth.onAuthStateChanged(() => {
	if (!app) {
		app = new Vue({
			router,
			store,
			apolloProvider,
			render: h => h(App)
		}).$mount("#app");
	}
});
