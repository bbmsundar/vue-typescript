import { Module, VuexModule, Action, Mutation } from "vuex-module-decorators";
import { auth } from "@/firebase/firebase";
import firebase from "firebase/app";

@Module({ namespaced: true, name: "auth", stateFactory: true })
export default class Auth extends VuexModule {
	count = 0;
	username = "";

	@Mutation
	updateUserName(name: string) {
		this.username = name;
	}

	@Action({ commit: "updateUserName" })
	async googleLogin() {
		await auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
		return auth.currentUser?.displayName;
	}
}
