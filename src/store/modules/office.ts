import { Module, VuexModule, Action, Mutation } from "vuex-module-decorators";
import { OFFICE, OFFICES, OFFICEQUERY } from "@/interfaces/office";
import apolloProvider from "@/apollo/index";
import { GETOFFICES } from "@/store/constants/office-constant";
import { ApolloQueryResult } from "apollo-client";
@Module({ namespaced: true, name: "office", stateFactory: true })
export default class Office extends VuexModule {
	office: OFFICE = {};
	offices: OFFICES = { data: [], count: 0 };

	@Mutation
	updateOffice(data: OFFICE) {
		this.office = data;
	}

	@Mutation
	updateOffices(data: OFFICES) {
		this.offices = data;
	}

	get getOffices() {
		return this.offices;
	}

	@Action({ commit: "updateOffices" })
	async getOfficeFromServer(payload: OFFICEQUERY): Promise<ApolloQueryResult<OFFICES>> {
		const result = await apolloProvider.defaultClient.query({
			query: GETOFFICES,
			variables: payload,
			fetchPolicy: "network-only"
		});
		return result?.data?.offices;
	}
}
