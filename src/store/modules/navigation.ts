import { Module, VuexModule, Action, Mutation } from "vuex-module-decorators";
interface SubMenuType {
	[key: string]: {
		title?: string;
		items: { title: string; to: string }[];
	};
}
@Module({ namespaced: true, name: "navigation", stateFactory: true })
export default class Navigation extends VuexModule {
	topNavigationSubRoutes = [
		"partners-index-slug",
		"conversations-index-slug",
		"news-index-slug",
		"reports-index-slug",
		"Settings",
		"settings-manage",
		"Office"
	];
	subMenus: SubMenuType = {
		"partners-index-slug": {
			items: [
				{ title: "Institutions", to: "/partners/institutions" },
				{ title: "Outbound Agents", to: "/partners/outboundagents" },
				{ title: "Introducers", to: "/partners/introducers" },
				{ title: "Franchise", to: "/partners/franchise" }
			]
		},
		"conversations-index-slug": {
			items: [
				{ title: "Chat/Emails", to: "/conversations/chat" },
				{ title: "Calender", to: "/conversations/calendar" },
				{ title: "Tasks", to: "/conversations/tasks" }
			]
		},
		"news-index-slug": {
			items: [
				{ title: "Announcements", to: "/news/announcements" },
				{ title: "Articles", to: "/news/articles" }
			]
		},
		"reports-index-slug": {
			items: [
				{ title: "Create", to: "/reports/create" },
				{ title: "Analytics", to: "/reports/analytics" }
			]
		},
		Settings: {
			title: "Settings",
			items: [
				{ title: "Manage", to: "/settings/manage" },
				{ title: "Productivity", to: "/settings/productivity" },
				{ title: "Account", to: "/settings/account" }
			]
		},
		"settings-manage": {
			title: "Settings",
			items: [
				{ title: "Manage", to: "/settings/manage" },
				{ title: "Productivity", to: "/settings/productivity" },
				{ title: "Account", to: "/settings/account" }
			]
		},
		Office: {
			title: "Settings",
			items: [
				{ title: "Manage", to: "/settings/manage" },
				{ title: "Productivity", to: "/settings/productivity" },
				{ title: "Account", to: "/settings/account" }
			]
		}
	};
	currentRouteName = "";

	@Mutation
	setCurrentRouteName(route: string) {
		this.currentRouteName = route;
	}

	@Action({ commit: "setCurrentRouteName" })
	setCurrentRouteNameAction(route: string) {
		console.log("inside Action", route);
		return route;
	}

	get getCurrentRouteName() {
		return this.currentRouteName;
	}

	get hasTopNavigationSubNavs() {
		if (!this.currentRouteName) {
			return false;
		}

		return this.topNavigationSubRoutes.includes(this.currentRouteName);
	}

	get getSubNavigationItems() {
		if (!this.currentRouteName || !Object.keys(this.subMenus).includes(this.currentRouteName)) {
			return {
				items: []
			};
		}

		return this.subMenus[this.currentRouteName];
	}
}
