import gql from "graphql-tag";

export const GETOFFICES = gql`
	query offices($data: OfficePaginationInput!) {
		offices(data: $data) {
			data {
				_id
				name
				email
				contactNumber
				address {
					streetName
					area
					pincode
					suiteNumber
					city {
						id
						name
					}
				}
				status
				partner {
					id
					name
				}
				departments
				state {
					id
					name
				}
				country {
					id
					name
				}
				managedBy {
					_id
					name
					role {
						name
					}
					profilePic
				}
				createdBy {
					name
					_id
					role {
						name
					}
					profilePic
				}
				createdAt
				businessUnitType
			}
			count
		}
	}
`;

export const CREATEOFFICE = gql`
	mutation createOffice($data: OfficeInput!) {
		createOffice(data: $data)
	}
`;

export const UPDATEOFFICE = gql`
	mutation updateOffice($data: UpdateOfficeInput!) {
		updateOffice(data: $data)
	}
`;

export const DELETEOFFICE = gql`
	mutation deleteOffice($newId: ObjectId!, $oldId: ObjectId!) {
		deleteOffice(newId: $newId, oldId: $oldId)
	}
`;
