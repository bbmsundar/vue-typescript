import Vue from "vue";
import Vuex from "vuex";
import Auth from "./modules/auth";
import Office from "./modules/office";
import Navigation from "./modules/navigation";
Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		auth: Auth,
		office: Office,
		navigation: Navigation
	}
});
