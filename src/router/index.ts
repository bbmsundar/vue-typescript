import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import { auth } from "../firebase/firebase";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
	{
		path: "/",
		name: "Home",
		component: Home,
		meta: {
			requiresAuth: true,
			layout: "header"
		}
	},
	{
		path: "/about",
		name: "About",
		// route level code-splitting
		// this generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () => import(/* webpackChunkName: "about" */ "../views/About.vue"),
		meta: {
			requiresAuth: true,
			layout: "header"
		}
	},
	{
		path: "/login",
		name: "Login",
		// route level code-splitting
		// this generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () => import(/* webpackChunkName: "login" */ "../views/Login.vue"),
		meta: {
			layout: "login"
		}
	},
	{
		path: "/office",
		name: "Office",
		component: () => import(/* webpackChunkName: "office" */ "../views/Office.vue"),
		meta: {
			requiresAuth: true,
			layout: "header"
		}
	},
	{
		path: "/settings",
		name: "Settings",
		redirect: { name: "settings-manage" },
		component: () => import(/* webpackChunkName: "settings" */ "../views/Settings/index.vue"),
		children: [
			{
				path: "manage",
				name: "settings-manage",
				component: () => import(/* webpackChunkName: "settings" */ "../views/Settings/Manage.vue"),
				meta: {
					requiresAuth: true
				},
				redirect: { name: "settings-manage-offices" },
				children: [
					{
						path: "offices",
						name: "settings-manage-offices",
						component: () => import(/* webpackChunkName: "settingsmanage" */ "../views/Office.vue"),
						meta: {
							requiresAuth: true
						}
					},
					{
						path: "teams",
						name: "settings-manage-teams",
						component: () => import(/* webpackChunkName: "settingsmanage" */ "../views/Team.vue"),
						meta: {
							requiresAuth: true
						}
					},
					{
						path: "roles",
						name: "settings-manage-roles",
						component: () => import(/* webpackChunkName: "settingsmanage" */ "../views/Role.vue"),
						meta: {
							requiresAuth: true
						}
					},
					{
						path: "peoples",
						name: "settings-manage-peoples",
						component: () => import(/* webpackChunkName: "settingsmanage" */ "../views/People.vue"),
						meta: {
							requiresAuth: true
						}
					}
				]
			}
		]
	}
];

const router = new VueRouter({
	mode: "history",
	base: process.env.BASE_URL,
	routes
});

router.beforeEach((to, from, next) => {
	const requiresAuth = to.matched.some(x => x.meta.requiresAuth);

	if (requiresAuth && !auth.currentUser) {
		next("/login");
	} else {
		next();
	}
});

export default router;
