interface BaseName {
	id: string;
	name: string;
}
export interface OFFICE {
	_id?: string;
	name?: string;
	email?: string;
	contactNumber?: string;
	address?: {
		streetName?: string;
		area?: string;
		pincode?: string;
		suiteNumber?: string;
		city?: BaseName;
	};
	status?: string;
	partner?: BaseName;
	departments?: string;
	state?: BaseName;
	country?: BaseName;
	managedBy?: {
		_id?: string;
		name?: string;
		role?: {
			name?: string;
		};
		profilePic?: string;
	};
	createdBy?: {
		name?: string;
		_id?: string;
		role?: {
			name?: string;
		};
		profilePic?: string;
	};
	createdAt?: string;
	businessUnitType?: string;
}
export interface OFFICES {
	data: OFFICE[];
	count: number;
}

export interface OFFICEQUERY {
	data: {
		limit: number;
		skip: number;
		ascending: boolean;
		orderBy: string;
		query: {
			name: string;
		};
	};
}
